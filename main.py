#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
# 启动171服务 
cd /data/chainmaker/taifu/chainmaker-go-xty/scripts && ./cluster_quick_start.sh normal

# 执行当前脚本：
wsl
python main.py

"""
import time


import os
from google.protobuf import json_format
from chainmaker.chain_client import ChainClient, RequestError
from chainmaker.keys import RuntimeType


createContractTimeout    = 5
claimContractName        = "claim007_python"
claimVersion             = "2.0.0"
claimByteCodePath        = "./testdata/rust-fact-2.0.0.wasm"
sdkConfigOrg1Client1Path = "./testdata/sdk_config.yml"
claimQueryMethod  = "find_by_file_hash"
claimInvokeMethod = "save"
claimContractRuntimeType = "WASMER"

cc = ChainClient.from_conf('./testdata/sdk_config.yml')


def contract_exists(cc, contract_name: str):
    """检查合约是否已安装"""
    try:
        cc.query_contract(contract_name, None)
    except RequestError as ex:
        if ex.err_code == 'INVALID_CONTRACT_PARAMETER_CONTRACT_NAME':
            return False
    return True

def create_contract(contract_name: str, version: str, byte_code_path: str, runtime_type: RuntimeType, params: dict = None, 
                    with_sync_result=True) -> dict:
    """创建合约"""
    # 创建请求payload
    payload = cc.create_contract_create_payload(contract_name, version, byte_code_path, runtime_type, params)
    # 创建背书
    # endorsers = cc.create_endorsers(payload, endorsers_config)
    # 携带背书发送请求
    res = cc.send_request_with_sync_result(payload, with_sync_result=with_sync_result)
    # 交易响应结构体转为字典格式
    return json_format.MessageToDict(res)

if not contract_exists(cc, claimContractName):
    print("合约不存在，开始创建合约"+claimContractName)
    # 创建WASM合约，本地合约文件./testdata/claim-wasm-demo/rust-fact-2.0.0.wasm应存在
    result1 = create_contract(claimContractName, claimVersion, claimByteCodePath, RuntimeType.WASMER, {})
    print(result1)
else:
    print("合约已存在")

file_hash="ab3456df5799b"+'%f'%time.time()
# 调用WASM合约
print()
print("调用")
res1 = cc.invoke_contract(claimContractName, claimInvokeMethod, {"file_name":"name007","file_hash":file_hash,"time":"6543234"},with_sync_result=True)
# 交易响应结构体转为字典格式
print(json_format.MessageToDict(res1))

# 查询合约
print()
print("查询")
res = cc.query_contract(claimContractName, claimQueryMethod, {"file_hash":file_hash})
print(json_format.MessageToDict(res1))